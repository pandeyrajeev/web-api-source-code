﻿using System;
using System.Collections.Generic;
//using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CF.PMS.Data.IRepository;
using CF.PMS.Data.Models;
//using System.Data.Entity;

namespace CF.PMS.Data.DataManager
{
    /// <summary>
    /// A generic base class to perform all the standard crud 
    /// operations on the database.
    /// </summary>

    public class BaseRepository<T> : IDataRepository<T> where T : class
    {
        protected readonly CFContext _dbContext;
        private Microsoft.EntityFrameworkCore.DbSet<T> _dbSet;

        public BaseRepository(CFContext context)
        {
            this._dbContext = context;
            this._dbSet = context.Set<T>();
        }

        /// <summary>
        /// Get a list of the requestsed object.
        /// </summary>
        /// <returns>A list of object type t</returns>
        public IQueryable<T> GetAll()
        {
            return _dbSet.AsQueryable<T>();
        }

        public async Task<IQueryable<T>> GetAllAync()
        {
            return (await _dbSet.ToListAsync()).AsQueryable();
        }

        /// <summary>
        /// Gets a list of objects matching the predicate argument
        /// </summary>
        /// <param name="predicate">The argument</param>
        /// <returns>A list of object type t</returns>
        public IEnumerable<T> Find(Func<T, bool> predicate)
        {
            return _dbSet.Where(predicate);
        }

        /// <summary>
        /// Gets a single object.
        /// </summary>
        /// <param name="predicate">The argument</param>
        /// <returns>An object of type T</returns>
        public T Single(Func<T, bool> predicate)
        {
            return _dbSet.Single(predicate);
        }

        /// <summary>
        /// Get the object matching the predicate argument.
        /// </summary>
        /// <param name="predicate">The argument</param>
        /// <returns>An object of type T</returns>
        public T Get(Func<T, bool> predicate)
        {
            return _dbSet.FirstOrDefault(predicate);
        }

        /// <summary>
        /// Gets an object via the guid id.
        /// </summary>
        /// <param name="id">The guid of the object</param>
        /// <returns>The object of type T</returns>
        public async Task<T> Get(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        /// <summary>
        /// Gets an object via the object reference.
        /// </summary>
        /// <param name="obj">The object to search for</param>
        /// <returns>The object of type T</returns>
        public async Task<T> Get(T obj)
        {
            return await _dbSet.FindAsync(obj);
        }

        #region async calls
        /// <summary>
        /// Saves an object to the database.
        /// </summary>
        /// <param name="t">The object to save</param>
        /// <returns></returns>
        public async Task<int> AddAsync(T t)
        {
            _dbContext.Set<T>().Add(t);
            return await _dbContext.SaveChangesAsync();

        }

        /// <summary>
        /// Removes an object from database
        /// </summary>
        /// <param name="t">The object to remove</param>
        /// <returns></returns>
        public async Task<int> RemoveAsync(T t)
        {
            _dbContext.Entry(t).State = EntityState.Deleted;
            return await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes an object from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<int> Delete(int id)
        {

            var objToDelete = _dbSet.Find(id);
            if (objToDelete == null)
            {
                throw new ArgumentNullException($"{nameof(objToDelete)} is null");
            }
            _dbSet.Remove(objToDelete);
            return await _dbContext.SaveChangesAsync();

        }

        public async Task<int> UpdateAsyn(T t, int key)
        {
            if (t == null)
            {
                throw new ArgumentNullException($"{nameof(t)} is null");
            }
            T exist = await _dbContext.Set<T>().FindAsync(key);
            if (exist != null)
            {
                _dbContext.Entry(exist).CurrentValues.SetValues(t);
                return await _dbContext.SaveChangesAsync();
            }
            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<int> CountAsync()
        {
            return await _dbContext.Set<T>().CountAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<T>> GetAllAsync()
        {
            return await _dbContext.Set<T>().ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="match"></param>
        /// <returns></returns>
        public async Task<T> FindAsync(Expression<Func<T, bool>> match)
        {
            return await _dbContext.Set<T>().SingleOrDefaultAsync(match);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="match"></param>
        /// <returns></returns>
        public async Task<List<T>> FindAllAsync(Expression<Func<T, bool>> match)
        {
            return await _dbContext.Set<T>().Where(match).ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="match"></param>
        /// <returns></returns>
        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }
        #endregion
    }
}
