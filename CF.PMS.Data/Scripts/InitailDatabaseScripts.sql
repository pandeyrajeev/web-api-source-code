--1. Run migrations for Identity

ALTER TABLE [AspNetUsers] ADD [ConcurrencyStamp] nvarchar(max) NULL;

GO

ALTER TABLE [AspNetUsers] ADD [LockoutEnd] datetimeoffset NULL;

GO

ALTER TABLE [AspNetUsers] ADD [NormalizedEmail] nvarchar(max) NULL;

GO

ALTER TABLE [AspNetUsers] ADD [NormalizedUserName] nvarchar(max) NULL;

GO

ALTER TABLE [AspNetRoles] ADD [ConcurrencyStamp] nvarchar(max) NULL;

GO

ALTER TABLE [AspNetRoles] ADD [NormalizedName] nvarchar(max) NULL;

GO


--2. Update Identity Tables

update [dbo].[AspNetUsers]
set [NormalizedEmail]=[Email],
[NormalizedUserName]=[UserName]

update [dbo].[AspNetRoles]
set [NormalizedName]=[Name]

GO

--3. Add Credit column
--ALTER TABLE [dbo].[AspNetUsers] ADD [Credits] [int] NOT NULL DEFAULT 0
--GO
