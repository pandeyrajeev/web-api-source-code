﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.Data.Exceptions
{
    /// <summary>
    /// Error Item
    /// </summary>
    public class ErrorItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorItem"/> class.
        /// </summary>
        protected ErrorItem()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorItem"/> class.
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="message">Message</param>
        public ErrorItem(string key, string message)
        {
            this.Key = key;
            this.Message = message;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorItem"/> class.
        /// </summary>
        /// <param name="message">Message</param>
        public ErrorItem(string message)
            : this(string.Empty, message)
        {
        }

        /// <summary>
        /// Gets or sets key
        /// </summary>
        public string Key { get; protected set; }

        /// <summary>
        /// Gets or sets message
        /// </summary>
        public string Message { get; protected set; }
    }
}
