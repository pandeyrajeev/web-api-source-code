﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CF.PMS.Data.Extensions;

namespace CF.PMS.Data.Exceptions
{
    /// <summary>
    /// BusinessException
    /// </summary>
    public class BusinessException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessException"/> class.
        /// </summary>
        /// <param name="messgae">messgae</param>
        public BusinessException(string messgae)
            : base(messgae)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessException" /> class.
        /// </summary>
        public BusinessException()
            : this("The system has encountered a technical problem")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessException"/> class.
        /// </summary>
        /// <param name="errors">errors</param>
        public BusinessException(params string[] errors)
            : this(errors.ToArray().Join("\\n"))
        {
        }

        /// <summary>
        /// Gets errors
        /// </summary>
        public List<ErrorItem> Errors { get; } = new List<ErrorItem>();

        /// <summary>
        /// Gets a value indicating whether any errors
        /// </summary>
        public bool HasError => this.Errors.Any() && this.Errors.Count != 0;

        /// <summary>
        /// AddException
        /// </summary>
        /// <param name="message">message</param>
        public void AddException(string message)
        {
            if (!message.IsEmpty())
            {
                this.Errors.Add(new ErrorItem(message));
            }
        }

        /// <summary>
        /// AddException
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="message">message</param>
        public void AddException(string key, string message)
        {
            if (!message.IsEmpty() && !key.IsEmpty())
            {
                this.Errors.Add(new ErrorItem(key, message));
            }
        }
    }
}
