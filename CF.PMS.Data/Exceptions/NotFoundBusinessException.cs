﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.Data.Exceptions
{
    /// <summary>
    /// NotFoundBusinessException
    /// </summary>
    public class NotFoundBusinessException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotFoundBusinessException"/> class.
        /// </summary>
        /// <param name="message">message</param>
        public NotFoundBusinessException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotFoundBusinessException"/> class.
        /// </summary>
        public NotFoundBusinessException()
        {
        }
    }
}
