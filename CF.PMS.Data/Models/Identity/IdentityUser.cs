﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CF.PMS.Data.Models
{
    public class ApplicationUser : IdentityUser
    {
        public bool IsActive { get; set; }
        public string CountryCode { get; set; }
    }
    public partial class AspNetUsers : IdentityUser
    {
       
    }
}
