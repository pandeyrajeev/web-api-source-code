﻿using System;
using System.Collections.Generic;

namespace CF.PMS.Data.Models
{
    public partial class LogItems
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Level { get; set; }
        public string Message { get; set; }
        public string Client { get; set; }
        public string Url { get; set; }
    }
}
