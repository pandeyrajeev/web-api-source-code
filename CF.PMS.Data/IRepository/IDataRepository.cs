﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CF.PMS.Data.IRepository
{
    public interface IDataRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        Task<IQueryable<T>> GetAllAync();
        IEnumerable<T> Find(Func<T, bool> predicate);
        T Single(Func<T, bool> predicate);
        T Get(Func<T, bool> predicate);
        Task<T> Get(int id);
        Task<T> Get(T obj);
        Task<int> Delete(int id);
        Task<int> AddAsync(T t);
        Task<int> RemoveAsync(T t);
        Task<List<T>> GetAllAsync();
        //Task<int> UpdateAsync(T t, object key);
        Task<int> UpdateAsyn(T t, int key);
        Task<int> CountAsync();
        Task<T> FindAsync(Expression<Func<T, bool>> match);
        Task<List<T>> FindAllAsync(Expression<Func<T, bool>> match);
        int SaveChanges();
    }
}
