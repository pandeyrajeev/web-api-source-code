﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.Data.Extensions
{
    /// <summary>
    /// Extensions class.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Check whether given value is empty or not
        /// </summary>
        /// <param name="text">string value</param>
        /// <returns><see cref="bool"/></returns>
        public static bool IsEmpty(this string text)
        {
            return string.IsNullOrEmpty(text) || string.IsNullOrWhiteSpace(text);
        }
    }
}

