﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CF.PMS.Data.IRepository;
using CF.PMS.Data.Models;

namespace CF.PMS.API.Infrastructure
{
    /// <summary>
    /// Log Type
    /// </summary>
    public enum LogType
    {
        /// <summary>
        /// Info
        /// </summary>
        Info,

        /// <summary>
        /// Warning
        /// </summary>
        Warning,

        /// <summary>
        /// Error
        /// </summary>
        Error
    }

    /// <summary>
    /// Logger
    /// </summary>
    public class CustomLogger : ICustomLogger
    {
        private IDataRepository<LogItems> logRepo;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logRepo">logRepo</param>
        public CustomLogger(IDataRepository<LogItems> logRepo)
        {
            this.logRepo = logRepo;
        }

        /// <summary>
        /// Log Information
        /// </summary>
        /// <param name="log"></param>
        public void LogInfo(LogItems log)
        {
            SaveLog(log);
        }

        /// <summary>
        /// Log Information
        /// </summary>
        /// <param name="message"></param>
        public void LogInfo(string message)
        {
            var log = new LogItems
            {
                Client = string.Empty,
                Date = DateTime.Now,
                Level = LogType.Info.ToString().ToUpper(),
                Message = message,
                Url = string.Empty
            };
            SaveLog(log);
        }

        /// <summary>
        /// Log Warning
        /// </summary>
        /// <param name="message"></param>
        public void LogWarning(string message)
        {
            var log = new LogItems
            {
                Client = string.Empty,
                Date = DateTime.Now,
                Level = LogType.Warning.ToString().ToUpper(),
                Message = message,
                Url = string.Empty
            };
            SaveLog(log);
        }

        /// <summary>
        /// Log Error
        /// </summary>
        /// <param name="message"></param>
        public void LogError(string message)
        {
            var log = new LogItems
            {
                Client = string.Empty,
                Date = DateTime.Now,
                Level = LogType.Error.ToString().ToUpper(),
                Message = message,
                Url = string.Empty
            };
            SaveLog(log);
        }

        /// <summary>
        /// Log Error
        /// </summary>
        /// <param name="ex"></param>
        public void LogError(Exception ex)
        {
            LogError(ex, string.Empty);
        }

        /// <summary>
        /// Log Error
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        public void LogError(Exception ex, string message)
        {
            var exceptionMessage = GetExceptionDetails(ex, string.Empty);
            exceptionMessage = message + " " + exceptionMessage;
            var log = new LogItems
            {
                Client = string.Empty,
                Date = DateTime.Now,
                Level = LogType.Info.ToString().ToUpper(),
                Message = exceptionMessage,
                Url = string.Empty
            };
            SaveLog(log);
        }

        /// <summary>
        /// Log Error
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        /// <param name="log"></param>
        public void LogError(Exception ex, string message, LogItems log)
        {
            var exceptionMessage = GetExceptionDetails(ex, string.Empty);
            exceptionMessage = message + " " + exceptionMessage;
            log.Message = exceptionMessage;
            log.Level = LogType.Error.ToString().ToUpper();
            SaveLog(log);
        }

        /// <summary>
        /// GetExceptionDetails
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private string GetExceptionDetails(Exception ex, string message)
        {
            if (ex != null)
            {
                string messageDetails = ex.Message;
                string stackTrace = ex.StackTrace;

                message += $"{messageDetails} {stackTrace}";
            }

            if (ex.InnerException != null)
            {
                return GetExceptionDetails(ex.InnerException, message);
            }
            return message;
        }

        /// <summary>
        /// Save Log
        /// </summary>
        /// <param name="log"></param>
        private void SaveLog(LogItems log)
        {
            try
            {
                var result = logRepo.AddAsync(log).Result;
                logRepo.SaveChanges();
            }
            catch (Exception)
            {
                //don't do anything 
            }
        }
    }


    /// <summary>
    /// Logger
    /// </summary>
    public interface ICustomLogger
    {
        /// <summary>
        /// Log Information
        /// </summary>
        /// <param name="log"></param>
        void LogInfo(LogItems log);

        /// <summary>
        /// Log Information
        /// </summary>
        /// <param name="message"></param>
        void LogInfo(string message);

        /// <summary>
        /// Log Warning
        /// </summary>
        /// <param name="message"></param>
        void LogWarning(string message);

        /// <summary>
        /// Log Error
        /// </summary>
        /// <param name="message"></param>
        void LogError(string message);

        /// <summary>
        /// Log Error
        /// </summary>
        /// <param name="ex"></param>
        void LogError(Exception ex);

        /// <summary>
        /// Log Error
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        void LogError(Exception ex, string message);

        /// <summary>
        /// Log Error
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        /// <param name="log"></param>
        void LogError(Exception ex, string message, LogItems log);
    }
}
