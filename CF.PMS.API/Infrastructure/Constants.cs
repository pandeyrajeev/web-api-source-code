﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.Infrastructure
{
    /// <summary>
    /// Constants
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// Available roles in the system
        /// </summary>
        public class Roles
        {
            /// <summary>
            /// RegularUser role key
            /// </summary>
            public const string RegularUser = "RegularUser";

            /// <summary>
            /// Administrator role key
            /// </summary>
            public const string Administrator = "Administrator";

            /// <summary>
            /// Urenschrijver role key
            /// </summary>
            public const string Urenschrijver = "Urenschrijver";

            /// <summary>
            /// FinancieleAdministratie role key
            /// </summary>
            public const string FinancieleAdministratie = "Financiele administratie";

            /// <summary>
            /// Directie role key
            /// </summary>
            public const string Directie = "Directie";           

            /// <summary>
            /// Projectleider role key
            /// </summary>
            public const string Projectleider = "Projectleider";

            /// <summary>
            /// Teamleider role key
            /// </summary>
            public const string Teamleider = "Teamleider";
            /// <summary>
            /// String role key
            /// </summary>
            public const string Producent = "Producent";

        }
    }
}
