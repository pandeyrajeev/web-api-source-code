﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.Infrastructure
{
    /// <summary>
    /// All the messages used in the system
    /// </summary>
    public class Resources
    {
        /// <summary>
        /// EmployeeStandardsControllerResources
        /// </summary>
        public class EmployeeStandardsControllerResources
        {
            /// <summary>
            /// Error message to show when employee doesn't exists.
            /// </summary>
            public const string EmployeeNotExistingErrorMessage = "Employee with Id:{0} doesn't exists.";

            /// <summary>
            /// Error message to show when Given year is not valid
            /// </summary>
            public const string YearNotValidErrorMessage = "Given Year:{0} is not valid, please provide a value between 2018 and 2050";

            /// <summary>
            /// Error message to show when role doesn't exists.
            /// </summary>
            public const string RoleNotExistingErrorMessage = "Role with Id:{0} doesn't exists.";

            /// <summary>
            /// Error message to show when employee and year doesn't exists.
            /// </summary>
            public const string EmployeeYearCombinationAlreadyExistsErrorMessage = "The given Employee Id:{0} and Year:{1} combination already exists.";
        }

        /// <summary>
        /// HourTypeControllerResources
        /// </summary>
        public class HourTypeControllerResources
        {
            /// <summary>
            /// Error message to show when Hour Type already exists.
            /// </summary>
            public const string HourTypeAlreadyExistsErrorMessage = "Hour Type with name:{0} already exists.";

            /// <summary>
            /// Error message to show when Hour type is not enter...
            /// </summary>
            public const string HourTypeRequiredMessage = "Hour Type is required.";
        }


        /// <summary>
        /// SettingsControllerResources
        /// </summary>
        public class SettingsControllerResources
        {
            /// <summary>
            /// Error message to show when label/value combination already exists.
            /// </summary>
            public const string LabelValueCombinationAlreadyExistsErrorMessage = "The given combination for Label:{0} and Value:{1} already exists.";

        }

        /// <summary>
        /// EmployeeControllerResources
        /// </summary>
        public class EmployeeControllerResources
        {
            /// <summary>
            /// Error message to show when email already exists.
            /// </summary>
            public const string EmailAlreadyErrorMessage = "Employee with Email:{0} already exists.";

        }

        /// <summary>
        /// UserControllerResources
        /// </summary>
        public class ProjectRoleControllerResources
        {
            /// <summary>
            /// Error message to show when role name doesn't exists.
            /// </summary>
            public const string RoleNotExistingErrorMessage = "Role with Name :{0} doesn't exists.";
        }

        /// <summary>
        /// UserControllerResources
        /// </summary>
        public class UserControllerResources
        {
            /// <summary>
            /// Error message to show when role name doesn't exists.
            /// </summary>
            public const string RoleNotExistingErrorMessage = "Role with Name :{0} doesn't exists.";
        }

        /// <summary>
        /// CustomerControllerResources
        /// </summary>
        public class CustomerControllerResources
        {
            /// <summary>
            /// Error message to show when customer doesn't exists.
            /// </summary>
            public const string CustomerNotExistingErrorMessage = "Customer with Id:{0} doesn't exists.";
        }

        /// <summary>
        /// CustomerControllerResources
        /// </summary>
        public class AppointmentsControllerResources
        {
            /// <summary>
            /// Error message to show when Appointment Id doesn't exists.
            /// </summary>
            public const string AppointmentsNotExistingErrorMessage = "Appointment with Id:{0} doesn't exists.";

        }

        /// <summary>
        /// TokenControllerResources
        /// </summary>
        public class TokenControllerResources
        {
            /// <summary>
            /// Error message to show when token generation fails
            /// </summary>
            public const string TokenErrorMessage = "Username/Password mismatch.";
        }

        /// <summary>
        /// ZenoControllerResources
        /// </summary>
        public class ZenoControllerResources
        {
            /// <summary>
            /// Error message to show when exploitation doesn't exists
            /// </summary>
            public const string ExploitationNotExistingErrorMessage = "Given Exploitation with Id:{0} doesn't exists.";
        }

        /// <summary>
        /// VisitReportControllerResources
        /// </summary>
        public class DatabaseErrorControllerResources
        {

            /// <summary>
            /// Error message to show when Some error on getting data from database ...
            /// </summary>
            public const string GetDataFromDatabaseErrorMessage = "Some error on getting data from database.";

            /// <summary>
            /// Error message to show when Some error on saving data in database ...
            /// </summary>
            public const string SaveDataInDatabaseErrorMessage = "Some error on getting data from database.";
        }
    }
}
