﻿using CF.PMS.API.Infrastructure;
using CF.PMS.Data.Exceptions;
using CF.PMS.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.Filters
{
    /// <summary>
    /// GlobalExceptionHandler
    /// </summary>
    public class GlobalExceptionHandler : ExceptionFilterAttribute
    {
        private ICustomLogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalExceptionHandler"/> class.
        /// </summary>
        public GlobalExceptionHandler(ICustomLogger logger)
        {
            this.logger = logger;
        }

        /// <inheritdoc/>
        public override void OnException(ExceptionContext context)
        {
            if (context?.Exception != null)
            {
                switch (context.Exception)
                {
                    case BusinessException exception when exception.HasError:
                        foreach (var item in exception.Errors)
                        {
                            var key = item.Key ?? string.Empty;
                            context.ModelState.AddModelError(key, item.Message);
                        }

                        context.Result = new BadRequestObjectResult(context.ModelState);
                        context.ExceptionHandled = true;
                        break;
                    case BusinessException exception when !exception.HasError:
                        context.ModelState.AddModelError("Error", exception.Message);
                        context.Result = new BadRequestObjectResult(context.ModelState);
                        context.ExceptionHandled = true;
                        break;
                    case NotFoundBusinessException _:
                        context.Result = new NotFoundResult();
                        context.ExceptionHandled = true;
                        break;
                    default:
                        //log the exception
                        string referenceId = Guid.NewGuid().ToString();
                        var message = $"An unhandled exception occurred, please contact administartor with ReferenceId:{referenceId}";
                        var exceptionResult = new ObjectResult(new { message = message });
                        exceptionResult.StatusCode = 500;
                        context.Result = exceptionResult;

                        var client = context.HttpContext == null ? "" : context.HttpContext.Request.Host.ToString();
                        var url = context.HttpContext == null ? "" : context.HttpContext.Request.Path.ToString();

                        LogItems log = new LogItems();
                        log.Client = client;
                        log.Url = url;

                        logger.LogError(context.Exception, referenceId, log);

                        break;
                }

            }

            base.OnException(context);
        }
    }
}
