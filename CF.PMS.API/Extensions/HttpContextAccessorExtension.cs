﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.Extensions
{
    /// <summary>
    /// HttpContextAccessorExtension
    /// </summary>
    public static class IHttpContextAccessorExtension
    {
        /// <summary>
        /// Get the current user id from token
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        /// <returns></returns>
        public static string CurrentUser(this IHttpContextAccessor httpContextAccessor)
        {
            var stringId = httpContextAccessor?.HttpContext?.User?.FindFirst(JwtRegisteredClaimNames.Jti)?.Value;
            //int.TryParse(stringId ?? "0", out int userId);

            //return userId;
            return stringId;
        }
    }
}
