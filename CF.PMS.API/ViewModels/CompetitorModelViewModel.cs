﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.ViewModels
{
    public class CompetitorModelViewModel
    {

        /// <summary>
        /// Model Nummer
        /// </summary>
        public int? ModelNummer { get; set; }

        /// <summary>
        /// Model Oppervlakte
        /// </summary>
        public double? ModelOppervlakte { get; set; }

        /// <summary>
        /// Model Afmeting
        /// </summary>
        public string ModelAfmeting { get; set; }
    }
}
