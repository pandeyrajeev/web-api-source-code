﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.ViewModels
{
    /// <summary>
    /// StepViewModel class use for Question and QuestionIdoptions...
    /// </summary>
    public class StepViewModel
    {

        /// <summary>
        /// Question Id
        /// </summary>
        public int QuestionId { get; set; }

        /// <summary>
        /// Question Text
        /// </summary>
        public string QuestionText { get; set; }

        /// <summary>
        /// Option Id
        /// </summary>
        public int OptionId { get; set; }

        /// <summary>
        /// Question Option Text
        /// </summary>
        public string QuestionOptionText { get; set; }
    }
}
