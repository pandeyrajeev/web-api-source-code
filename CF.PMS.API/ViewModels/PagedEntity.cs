﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.ViewModels
{
    /// <summary>
    /// Wrapper for getting paged records
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PagedEntity<T>
    {
        /// <summary>
        /// Data to be returned
        /// </summary>
        public IEnumerable<T> Data { get; set; }

        /// <summary>
        /// Total number of records
        /// </summary>
        public int TotalCount { get; set; }
    }
}
