﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.ViewModels
{
    /// <summary>
    /// Available Roles
    /// </summary>
    public enum RoleEnum
    {
        /// <summary>
        /// User Role : The role with least permissions
        /// </summary>
        RegularUser,

        /// <summary>
        /// Urenschrijver
        /// </summary>
        Urenschrijver,

        /// <summary>
        /// FinancieleAdministratie
        /// </summary>
        FinancieleAdministratie,

        /// <summary>
        /// Directie
        /// </summary>
        Directie,

        /// <summary>
        /// Projectleider
        /// </summary>
        Projectleider,

        /// <summary>
        /// Teamleider
        /// </summary>
        Teamleider,

        /// <summary>
        /// Administrator
        /// </summary>
        Administrator,

        /// <summary>
        /// Producent
        /// </summary>
        Producent
    }
}
