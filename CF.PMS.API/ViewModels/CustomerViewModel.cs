﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.ViewModels
{
    /// <summary>
    /// Customer records
    /// </summary>
    public class CustomerViewModel
    {

        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Zip code
        /// </summary>
        public string Zipcode { get; set; }

        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// IsCustomer
        /// </summary>
        public bool IsCustomer { get; set; }


        /// <summary>
        /// TotalRows
        /// </summary>
        public int TotalRows { get; set; }

    }
    /// <summary>
    /// top 10 customer
    /// </summary>
    public class TopCustomers
    {
        /// <summary>
        /// revenue
        /// </summary>
        public int Revenue { get; set; }

        /// <summary>
        /// customer
        /// </summary>
        public string Customer { get; set; }
    }

    /// <summary>
    /// top items
    /// </summary>
    public class TopItems
    {
        /// <summary>
        /// revenue
        /// </summary>
        public int Revenue { get; set; }

        /// <summary>
        /// item
        /// </summary>
        public string Item { get; set; }
    }

    /// <summary>
    /// top model
    /// </summary>
    public class TopModels
    {
        /// <summary>
        /// revenue
        /// </summary>
        public int Revenue { get; set; }

        /// <summary>
        /// model
        /// </summary>
        public string Model { get; set; }
    }
    /// <summary>
    /// bind dropdown data...
    /// </summary>
    public class DropdownViewModel
    {

        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Text
        /// </summary>
        public string Text { get; set; }
    }
}
