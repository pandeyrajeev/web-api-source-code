﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.ViewModels
{
    public class VisitReportViewModel
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string UserId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
    }
}
