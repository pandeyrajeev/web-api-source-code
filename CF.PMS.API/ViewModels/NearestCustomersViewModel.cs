﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.ViewModels
{
    /// <summary>
    /// NearestCustomersViewModel model class
    /// </summary>
    public class NearestCustomersViewModel
    {
        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Distance
        /// </summary>
        public string Distance { get; set; }
    }
}
