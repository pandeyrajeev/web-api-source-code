﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.ViewModels
{
    /// <summary>
    /// Token Viewmodel
    /// </summary>
    public class TokenViewModel
    {
        /// <summary>
        /// Claims for the user
        /// </summary>
        public Dictionary<string, string> Claims { get; set; }

        /// <summary>
        /// Token for the user
        /// </summary>
        public string Token { get; set; }
    }
}
