﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.ViewModels
{
    /// <summary>
    /// AppointmentViewModel model class...
    /// </summary>
    public partial class AppointmentViewModel : AppointmentBaseViewModel
    {

        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
    }

    /// <summary>
    /// AppointmentBaseViewModel model class...
    /// </summary>
    public class AppointmentBaseViewModel
    {

        /// <summary>
        /// Subject
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Employee Id
        /// </summary>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public int? CustomerId { get; set; }

        /// <summary>
        /// Comment write by customerId
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Appointment Date
        /// </summary>
        public DateTime AppointmentDate { get; set; }


        /// <summary>
        /// Created By
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Appointment Date
        /// </summary>
        public string AppointmentsDate { get; set; }
    }
}
