﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.ViewModels
{
    public class QuestionViewModel
    {
        public int? QuestionId { get; set; }
        public int StepId { get; set; }
        public string QuestionText { get; set; }
    }
    public class OptionViewModel: QuestionViewModel
    {
        public int? OptionId { get; set; }
        public string OptionText { get; set; }
        public int? FlagOptionId { get; set; }

    }
    public class DropDownViewModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
}
