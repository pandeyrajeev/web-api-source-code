﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.ViewModels
{
    /// <summary>
    /// VisitStepViewModel model class...
    /// </summary>
    public class VisitStepViewModel
    {
        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// User Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Question Id
        /// </summary>
        public int QuestionId { get; set; }

        /// <summary>
        /// Answer Id
        /// </summary>
        public int? OptionID { get; set; }

        /// <summary>
        /// Step created Date ....
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Remark FOR Free text ....
        /// </summary>
        public string Remark { get; set; }
    }
}
