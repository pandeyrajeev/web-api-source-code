﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.ViewModels
{
    /// <summary>
    /// Role
    /// </summary>
    public class RoleViewModel
    {
        /// <summary>
        /// Unique Id of the Role
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Name for the Role
        /// </summary>
        public string Name { get; set; }
    }
}
