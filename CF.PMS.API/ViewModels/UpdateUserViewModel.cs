﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.ViewModels
{
    /// <summary>
    /// UpdateUserViewModel
    /// </summary>
    public class UpdateUserViewModel
    {
        /// <summary>
        /// Unique User Id of the user
        /// </summary>
        [Required]
        public string UserId { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// Confirm Password
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// Role to which the user has to be mapped
        /// </summary>
        [Required]
        public string RoleName { get; set; }
    }
}
