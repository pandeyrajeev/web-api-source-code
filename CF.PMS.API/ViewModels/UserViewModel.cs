﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.ViewModels
{
    /// <summary>
    /// User Details
    /// </summary>
    public class UserViewModel
    {
        /// <summary>
        /// Unique Id for the user
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Indicating if the user is active or not
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Email for the user
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Indicating if the Email Is confirmed
        /// </summary>
        public bool EmailConfirmed { get; set; }

        /// <summary>
        /// Phone number of the user
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Indicating if the Phone Number Is confirmed
        /// </summary>
        public bool PhoneNumberConfirmed { get; set; }
        /// <summary>
        /// Unique User Name of the user
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Country Code
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Credits Remaining for the user
        /// </summary>
        public int Credits { get; set; }

        /// <summary>
        /// Unique RoleId of the user
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// Role Name 
        /// </summary>
        public string RoleName { get; set; }
    }
}

