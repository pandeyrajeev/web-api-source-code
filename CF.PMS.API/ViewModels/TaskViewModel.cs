﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CF.PMS.API.ViewModels
{

    /// <summary>
    /// TaskViewModel model class...
    /// </summary>
    public partial class TaskViewModel : TaskBaseViewModel
    {

        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Left Day
        /// </summary>
        public int LeftDay { get; set; }
    }

    /// <summary>
    /// TaskBaseViewModel model class...
    /// </summary>
    public class TaskBaseViewModel
    {

        /// <summary>
        /// Subject
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public int? CustomerId { get; set; }

        /// <summary>
        /// User Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }
    }

    public class CreateTaskViewModel : TaskBaseViewModel
    {
        /// <summary>
        /// Status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// VisitDate
        /// </summary>
        public DateTime VisitDate { get; set; }

        /// <summary>
        /// Assigned By
        /// </summary>
        public string AssignedTo { get; set; }

        /// <summary>
        /// Created By
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Indirect
        /// </summary>
        public bool Indirect { get; set; }


    }
}
