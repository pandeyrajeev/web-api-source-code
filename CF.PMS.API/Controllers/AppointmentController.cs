﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CF.PMS.API.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using CF.PMS.API.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Data;

namespace CF.PMS.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Appointment")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Constants.Roles.Administrator)]
    public class AppointmentController : BaseController
    {
        /// <summary>
        /// Set the connection
        /// </summary>
        /// <param name="conn"></param>
        public AppointmentController(IOptions<ReadConfig> conn)
        {
            _conn = conn;
        }


        /// <summary>
        /// get all Appointments detail
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(int PageNumber = 1, int PageSize = 50)
        {
            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var parameters = new DynamicParameters();
                parameters.Add("@PageNumber", PageNumber);
                parameters.Add("@PageSize", PageSize);
                var result = await dbConnection.QueryAsync<AppointmentViewModel>("SP_KrijgenAfspraken", parameters, null, null, commandType: CommandType.StoredProcedure);
                if (result == null)
                {
                    return BadRequest();
                }
                return Ok(result);
            }
        }
        /// <summary>
        /// get Appointments detail by Appointmentid
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetById")]
        public async Task<IActionResult> GetById(int id)
        {
            string sqlQuery = @"SELECT  Id,CustomerId = KlantId,EmployeeId = WerknemerId,AppointmentDate = AfspraakDatum,[Subject] = Onderwerpen,Comment= Opmerkingen,CreatedBy= Gemaaktdoor from Afspraken where Id=" + id + "";

            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<AppointmentViewModel>(sqlQuery);
                if (result == null)
                {
                    return BadRequest();
                }
                return Ok(result);
            }
        }

        /// <summary>
        /// Get the Appointments by given user id
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByUserId/{userId}")]
        public async Task<IActionResult> GetByUserId(string userId)
        {
            string sqlQuery = @"SELECT  CustomerId = a.KlantId,EmployeeID = a.WerknemerId,[AppointmentsDate] = CONVERT(CHAR( 5),a.AfspraakDatum,114) +' '+ CONVERT(CHAR(11),a.AfspraakDatum,104),Comment = a.Opmerkingen,Subject=a.Onderwerpen,CreatedBy= a.Gemaaktdoor
                                from Afspraken a left join werknemers w on a.WerknemerId=w.[werknemer-ID] left join AspNetUsers ap on ap.email=w.[emailwerk] where a.Gemaaktdoor='" + userId + "'  and a.AfspraakDatum >=DATEADD([day], ((DATEDIFF([day], '19000101', getdate()) / 7) * 7) + 7, '19000101') " +
                                "and a.AfspraakDatum <=DATEADD([day], ((DATEDIFF([day], '19000101', getdate()) / 7) * 7) + 13, '19000101')   order by a.AfspraakDatum";

            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<AppointmentBaseViewModel>(sqlQuery);
                return Ok(result);
            }
        }

        /// <summary>
        /// get Appointment data from model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] AppointmentBaseViewModel model)
        {
            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                string CustSqlQuery = @"select	1 from klanten where Klantnummer =" + model.CustomerId + "";
                var CustResult = await dbConnection.QueryAsync(CustSqlQuery);
                if (CustResult.Count() == 0 || model.CustomerId==0)
                {
                    ModelState.AddModelError("", string.Format(Infrastructure.Resources.CustomerControllerResources.CustomerNotExistingErrorMessage, model.CustomerId));
                    return BadRequest(ModelState);
                }
                string EmpSqlQuery = @"select 1 from werknemers where [werknemer-Id] =" + model.EmployeeId + "";
                var EmpResult = await dbConnection.QueryAsync(EmpSqlQuery);
                if (EmpResult.Count() == 0 || model.EmployeeId =="0")
                {
                    ModelState.AddModelError("", string.Format(Infrastructure.Resources.EmployeeStandardsControllerResources.EmployeeNotExistingErrorMessage, model.EmployeeId));
                    return BadRequest(ModelState);
                }
                var parameters = new DynamicParameters();
                parameters.Add("@Onderwerpen", model.Subject);
                parameters.Add("@WerknemerId", model.EmployeeId);
                parameters.Add("@KlantId", model.CustomerId);
                parameters.Add("@Opmerkingen", model.Comment);
                parameters.Add("@AfspraakDatum", model.AppointmentDate);
                parameters.Add("@Gemaaktdoor", model.CreatedBy);
                var result = await dbConnection.ExecuteAsync("SP_CreërenBijwerkenAfspraken", parameters, null, null, commandType: CommandType.StoredProcedure);
                if (result == 1)
                {
                    var viewModel = GetCreateViewModel(model);
                    return Ok(viewModel);
                }
                else
                {
                    ModelState.AddModelError("Error", Resources.DatabaseErrorControllerResources.SaveDataInDatabaseErrorMessage);
                    return BadRequest(ModelState);
                }
            }
        }


        /// <summary>
        /// GetCreateViewModel method is use for assign value in AppointmentBaseViewModel
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private AppointmentBaseViewModel GetCreateViewModel(AppointmentBaseViewModel item)
        {
            AppointmentBaseViewModel viewModel = new AppointmentBaseViewModel();
            viewModel.Subject = item.Subject;
            viewModel.EmployeeId = item.EmployeeId;
            viewModel.CustomerId = item.CustomerId;
            viewModel.Comment = item.Comment;
            viewModel.AppointmentDate = item.AppointmentDate;
            viewModel.CreatedBy = item.CreatedBy;
            return viewModel;
        }


        /// <summary>
        /// get Appointment data from model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Update/{Id}")]
        public async Task<IActionResult> Update([FromBody] AppointmentViewModel model,int Id)
        {
            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                string CustSqlQuery = @"select	1 from klanten where Klantnummer ="+model.CustomerId+"";
                var CustResult = await dbConnection.QueryAsync(CustSqlQuery);
                if (CustResult.Count() == 0 || model.CustomerId == 0)
                {
                    ModelState.AddModelError("", string.Format(Infrastructure.Resources.CustomerControllerResources.CustomerNotExistingErrorMessage, model.CustomerId));
                    return BadRequest(ModelState);
                }
                string EmpSqlQuery = @"select 1 from werknemers where [werknemer-Id] ="+ model.EmployeeId + "";
                var EmpResult = await dbConnection.QueryAsync(EmpSqlQuery);
                if (EmpResult.Count() == 0 || model.EmployeeId == "0")
                {
                    ModelState.AddModelError("", string.Format(Infrastructure.Resources.EmployeeStandardsControllerResources.EmployeeNotExistingErrorMessage, model.EmployeeId));
                    return BadRequest(ModelState);
                }
                string AppointmentSqlQuery = @"select	1 from Afspraken where Id =" + Id + "";
                var AppointmentResult = await dbConnection.QueryAsync(AppointmentSqlQuery);
                if (AppointmentResult.Count() == 0 || Id == 0)
                {
                    ModelState.AddModelError("", string.Format(Infrastructure.Resources.AppointmentsControllerResources.AppointmentsNotExistingErrorMessage, Id));
                    return BadRequest(ModelState);
                }
                var parameters = new DynamicParameters();
                parameters.Add("@Onderwerpen", model.Subject);
                parameters.Add("@WerknemerId", model.EmployeeId);
                parameters.Add("@KlantId", model.CustomerId);
                parameters.Add("@Opmerkingen", model.Comment);
                parameters.Add("@AfspraakDatum", model.AppointmentDate);
                parameters.Add("@Id", Id);
                parameters.Add("@Gemaaktdoor", model.CreatedBy);
                var result = await dbConnection.ExecuteAsync("SP_CreërenBijwerkenAfspraken", parameters, null, null, commandType: CommandType.StoredProcedure);
                if (result == 1)
                {
                    var viewModel = GetUpdateViewModel(model, Id);
                    return Ok(viewModel);
                }
                else
                {
                    ModelState.AddModelError("Error", Resources.DatabaseErrorControllerResources.SaveDataInDatabaseErrorMessage);
                    return BadRequest(ModelState);
                }
            }
        }


        /// <summary>
        /// delete Appointments
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(int Id)
        {
            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                string AppointmentSqlQuery = @"select	1 from Afspraken where Id =" + Id + "";
                var AppointmentResult = await dbConnection.QueryAsync(AppointmentSqlQuery);
                if (AppointmentResult.Count() == 0 || Id == 0)
                {
                    ModelState.AddModelError("", string.Format(Infrastructure.Resources.AppointmentsControllerResources.AppointmentsNotExistingErrorMessage, Id));
                    return BadRequest(ModelState);
                }
                string SqlQuery = @"Delete from Afspraken where Id=" + Id + "";
                var result = await dbConnection.QueryAsync(SqlQuery);
                return Ok();
            }
        }

        /// <summary>
        /// GetUpdateViewModel method is use for assign value in AppointmentViewModel
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private AppointmentViewModel GetUpdateViewModel(AppointmentViewModel item,int Id)
        {
            AppointmentViewModel viewModel = new AppointmentViewModel();
            viewModel.Subject = item.Subject;
            viewModel.EmployeeId = item.EmployeeId;
            viewModel.CustomerId = item.CustomerId;
            viewModel.Comment = item.Comment;
            viewModel.AppointmentDate = item.AppointmentDate;
            viewModel.Id = Id;
            viewModel.CreatedBy = item.CreatedBy;
            return viewModel;
        }
    }
}