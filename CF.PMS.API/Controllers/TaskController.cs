﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CF.PMS.API.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using CF.PMS.API.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Data;

namespace CF.PMS.API.Controllers
{
    /// <summary>
    /// Task's API
    /// </summary>
    [Produces("application/json")]
    [Route("api/Task")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Constants.Roles.Administrator)]
    public class TaskController : BaseController
    {
        /// <summary>
        /// Set the connection
        /// </summary>
        /// <param name="conn"></param>
        public TaskController(IOptions<ReadConfig> conn)
        {
            _conn = conn;
        }

        /// <summary>
        /// Get the task by given id
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetById")]
        public async Task<IActionResult> GetById(int Id)
        {
            string sqlQuery = @"SELECT  Id = t.Id,Subject = Onderwerpen,UserId = t.Gebruikersnaam,CustomerId = t.KlantenID,Description= Beschrijving,14 - datediff(day,br.BezoekDatum,getdate()) as LeftDay
                                from Taak t left join BezoekRapport br on t.BezoekRapportId=br.Id where t.Id='" + Id + "'";

            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<TaskViewModel>(sqlQuery);
                return Ok(result);
            }
        }

        /// <summary>
        /// Get the task by given user id
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByUserId/{userId}")]
        public async Task<IActionResult> GetByUserId(string userId)
        {
            string sqlQuery = @"SELECT  Id = t.Id,Subject = Onderwerpen,UserId = t.Gemaaktdoor,CustomerId = t.KlantenID,Description= Beschrijving,14 - datediff(day,br.BezoekDatum,getdate()) as LeftDay
                                from Taak t left join BezoekRapport br on t.BezoekRapportId=br.Id where t.Gemaaktdoor='" + userId + "'";

            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<TaskViewModel>(sqlQuery);
                return Ok(result);
            }
        }

        /// <summary>
        /// Get task data from model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateTaskViewModel model)
        {
            using (System.Data.IDbConnection dbConnection = Connection)
            {
                bool Status = model.Status.ToUpper() == "OPEN" ? true : false;
                dbConnection.Open();
                var parameters = new DynamicParameters();
                parameters.Add("@Onderwerpen", model.Subject);
                parameters.Add("@KlantenID", model.CustomerId);
                parameters.Add("@Beschrijving", model.Description);
                parameters.Add("@Indirect", model.Indirect);
                parameters.Add("@staat", Status);
                parameters.Add("@BezoekDatum", model.VisitDate);
                parameters.Add("@Toegewezenaan", model.AssignedTo);
                parameters.Add("@Gemaaktdoor", model.CreatedBy);
                var result = await dbConnection.ExecuteAsync("SP_CreërenBezoekTaak", parameters, null, null, commandType: CommandType.StoredProcedure);
                if (result == 1)
                {
                    var viewModel = GetViewModel(model);
                    return Ok(viewModel);
                }
                else
                {
                    ModelState.AddModelError("Error", Resources.DatabaseErrorControllerResources.SaveDataInDatabaseErrorMessage);
                    return BadRequest(ModelState);
                }
            }
        }


        /// <summary>
        /// GetViewModel method is use for assign value in CreateTaskViewModel
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private CreateTaskViewModel GetViewModel(CreateTaskViewModel item)
        {
            CreateTaskViewModel viewModel = new CreateTaskViewModel();
            viewModel.Subject = item.Subject;
            viewModel.CustomerId = item.CustomerId;
            viewModel.Description = item.Description;
            viewModel.Status = item.Status;
            viewModel.VisitDate = item.VisitDate;
            viewModel.CreatedBy = item.CreatedBy;
            viewModel.AssignedTo = item.AssignedTo;
            return viewModel;
        }
    }
}