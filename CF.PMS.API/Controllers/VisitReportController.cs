﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CF.PMS.API.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using CF.PMS.API.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Data;

namespace CF.PMS.API.Controllers
{
    /// <summary>
    /// VisitReport's API
    /// </summary>
    [Produces("application/json")]
    [Route("api/VisitReport")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Constants.Roles.Administrator)]
    public class VisitReportController : BaseController
    {
        /// <summary>
        /// Set the connection
        /// </summary>
        /// <param name="conn"></param>
        public VisitReportController(IOptions<ReadConfig> conn)
        {
            _conn = conn;
        }

        /// <summary>
        /// Get the report by given user id
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByUserId/{userId}")]
        public async Task<IActionResult> GetByUserId(string userId)
        {
            string sqlQuery = @"SELECT  Id=br.Id,CustomerId = br.KlantenID, CustomerName = k.Klantnaam,Description = iif(t.Beschrijving=null,aw.Opmerking,t.Beschrijving),
                                [Date] = convert(varchar(12),br.BezoekDatum,104),UserId=br.Gebruikersnaam
                                from BezoekRapport br 
								inner join klanten k on k.Klantnummer=br.klantenId
								left join BezoekRapportVraagAntwoord aw on br.Id=aw.BezoekRapportId
                                left join Taak t on br.Id =t.BezoekRapportId
								where br.Gebruikersnaam='" + userId + "'  group by  br.Id,br.KlantenID,k.Klantnaam,t.Beschrijving,br.BezoekDatum,br.Gebruikersnaam";


            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<VisitReportViewModel>(sqlQuery);
                return Ok(result);
            }
        }

        /// <summary>
        /// Get the report by given Customer Id
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByCustomerId/{customerId}")]
        public async Task<IActionResult> GetByCustomerId(int customerId)
        {
            string sqlQuery = @"SELECT  Id=br.Id,CustomerId = br.KlantenID, CustomerName = k.Klantnaam,Description = iif(t.Beschrijving=null,aw.Opmerking,t.Beschrijving),
                                [Date] = convert(varchar(12),br.BezoekDatum,104),UserId=br.Gebruikersnaam
                                from BezoekRapport br 
								inner join klanten k on k.Klantnummer=br.klantenId
								left join BezoekRapportVraagAntwoord aw on br.Id=aw.BezoekRapportId
                                left join Taak t on br.Id =t.BezoekRapportId
								where br.KlantenID='" + customerId + "'  group by  br.Id,br.KlantenID,k.Klantnaam,t.Beschrijving,br.BezoekDatum,br.Gebruikersnaam";

            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<VisitReportViewModel>(sqlQuery);
                return Ok(result);
            }
        }

        /// <summary>
        /// get records by stepId from database...
        /// </summary>
        /// <param name="stepId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("VisitReport/{stepId}")]
        public async Task<IActionResult> VisitReport(int stepId = 2)
        {
            string sqlQuery = @"select QuestionId = q.VraagID,QuestionText=q.VragenTekst,OptionId=o.OptieID,QuestionOptionText=o.VraagoptieTekst  from Vragen q
                                              left join VraagOpties  o on q.VraagID =o.VraagID where q.StapID =" + stepId + "";

            if (!string.IsNullOrEmpty(sqlQuery))
            {
                using (System.Data.IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    var result = await dbConnection.QueryAsync<StepViewModel>(sqlQuery);
                    if (result == null)
                    {
                        return NotFound();
                    }
                    return Ok(result);
                }
            }
            ModelState.AddModelError("Error", Resources.DatabaseErrorControllerResources.GetDataFromDatabaseErrorMessage);
            return BadRequest(ModelState);
        }


        /// <summary>
        ///  Insert data in BezoekRapport table in database .....
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateVisitStep")]
        public async Task<IActionResult> CreateVisitStep([FromBody] VisitStepViewModel model)
        {
            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var parameters = new DynamicParameters();
                parameters.Add("@KLANTENID", model.CustomerId);
                parameters.Add("@VRAAGID", model.QuestionId);
                parameters.Add("@GEBRUIKERSNAAM", model.UserId);
                parameters.Add("@OPTIEID", model.OptionID);
                parameters.Add("@BezoekDatum", model.Date);
                parameters.Add("@Opmerking", model.Remark);
                var result = await dbConnection.ExecuteAsync("SP_CreërenBezoekStap", parameters, null, null, commandType: CommandType.StoredProcedure);
                if (result != -1)
                {
                    var viewModel = GetViewModel(model);
                    return Ok(viewModel);
                }
                else
                {
                    ModelState.AddModelError("Error", Resources.DatabaseErrorControllerResources.SaveDataInDatabaseErrorMessage);
                    return BadRequest(ModelState);
                }
            }
        }


        /// <summary>
        /// GetViewModel method is use for assign value in VisitStepViewModel
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private VisitStepViewModel GetViewModel(VisitStepViewModel item)
        {
            VisitStepViewModel viewModel = new VisitStepViewModel();
            viewModel.CustomerId = item.CustomerId;
            viewModel.UserId = item.UserId;
            viewModel.QuestionId = item.QuestionId;
            viewModel.OptionID = item.OptionID;
            viewModel.Date = item.Date;
            viewModel.Remark = item.Remark;
            return viewModel;
        }
    }
}