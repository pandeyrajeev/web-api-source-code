﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CF.PMS.API.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using CF.PMS.API.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace CF.PMS.API.Controllers
{
    /// <summary>
    /// Customer's API
    /// </summary>
    [Produces("application/json")]
    [Route("api/Customer")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Constants.Roles.Administrator)]
    public class CustomerController : BaseController
    {
        /// <summary>
        /// Set the connection
        /// </summary>
        /// <param name="conn"></param>
        public CustomerController(IOptions<ReadConfig> conn)
        {
            _conn = conn;
        }

        /// <summary>
        /// get all customer details
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(int PageNumber = 1, int PageSize = 50)
        {
            string sqlQuery = @"DECLARE @PageNumber INT = " + PageNumber + ", @PageSize INT = " + PageSize + " , @TotalRows INT=0; ";
            sqlQuery += @"select	Klantnummer as 'CustomerId',Klantnaam as 'Name',[Adresregel 1] as 'Address',Postcode as 'Zipcode',Woonplaats as 'City',IsKlant as 'IsCustomer' into #tmpklanten from klanten;";
            sqlQuery += @" select  @TotalRows =count(CustomerId) from #tmpklanten ;";
            sqlQuery += @"  WITH RTklanten   AS (SELECT *,Row_number() OVER(ORDER BY CustomerId) AS rownum FROM #tmpklanten) 
                                   SELECT *,@TotalRows as TotalRows FROM   RTklanten WHERE  rownum BETWEEN ( @PageNumber - 1 ) * @PageSize + 1 AND  @PageNumber * @PageSize  drop table #tmpklanten";
            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<CustomerViewModel>(sqlQuery);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
        }
        /// <summary>
        /// get customer detail by Customerid
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            string sqlQuery = @"select	Klantnummer as 'CustomerId',Klantnaam as 'Name',Telefoonnummer as 'Phone',Email as 'Email',Website as 'Website',[Adresregel 1] as 'Address',Postcode as 'Zipcode',
		                                Woonplaats as 'City',l.Omschrijving as 'Country',iif(IsKlant=1,'Active','NotActive') as 'IsCustomer' from klanten k  left join landen l on k.Landcode = l.Landcode where Klantnummer ="+ id + "";

            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<CustomerDetailViewModel>(sqlQuery);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
        }

        /// <summary>
        /// get top 10 customers
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTopCustomers")]
        public async Task<IActionResult> GetTopCustomers()
        {
            string sqlQuery = @"select top 10 round(sum(round(oi.FacPrEuro,0)),0) as Revenue, k.Klantnaam as Customer from Orders1 o
                              inner join Ordersinformatie oi on o.Ordernummer = oi.Ordernummer
                              inner join klanten k on o.Klantnummer = k.Klantnummer and k.Klantnaam <> 'CF kunststofprofielen'
                              group by k.Klantnaam
                              order by round(sum(round(oi.FacPrEuro,0)),0) desc";
            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<TopCustomers>(sqlQuery);
                return Ok(result);
            }
        }

        /// <summary>
        /// get top 10 items
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTopItems")]
        public async Task<IActionResult> GetTopItems()
        {
            string sqlQuery = @"select top 10 round(sum(round(oi.FacPrEuro,0)),0) as Revenue, a.Art_nr as Item from Orders1 o 
                              inner join Ordersinformatie oi on o.Ordernummer = oi.Ordernummer
                              inner join Artikels a on oi.Art_nr = a.Art_nr
                              group by a.Art_nr
                              order by round(sum(round(oi.FacPrEuro,0)),0) desc";
            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<TopItems>(sqlQuery);
                return Ok(result);
            }
        }

        /// <summary>
        /// get top 10 models
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTopModels")]
        public async Task<IActionResult> GetTopModels()
        {
            string sqlQuery = @"select top 10 round(sum(round(oi.FacPrEuro,0)),0) as Revenue, pm.Mod_Nummer as Model from Orders1 o
                              inner join Ordersinformatie oi on o.Ordernummer = oi.Ordernummer
                              inner join Artikels a on oi.Art_nr = a.Art_nr
                              inner join Profiel_Modellen pm on a.fk_Model = pm.Model_id
                              group by pm.Mod_Nummer
                              order by round(sum(round(oi.FacPrEuro,0)),0) desc";
            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<TopModels>(sqlQuery);
                return Ok(result);
            }
        }
        /// <summary>
        /// CustomerSearch method is use for search by customer name .... 
        /// </summary>
        /// <param name="custName"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("SearchCustomer/{custName}")]
        public async Task<IActionResult> SearchCustomer(string custName)
        {
            string sqlQuery = @"select	Klantnummer as 'Id',Klantnaam as 'Text' from klanten where Klantnaam like '"+ custName + "%'";
            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<DropdownViewModel>(sqlQuery);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
        }
    }
}