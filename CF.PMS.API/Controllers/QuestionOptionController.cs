﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CF.PMS.API.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using CF.PMS.API.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Data;

namespace CF.PMS.API.Controllers
{
    /// <summary>
    /// QuestionOption's API
    /// </summary>
    [Produces("application/json")]
    [Route("api/QuestionOption")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Constants.Roles.Administrator)]
    public class QuestionOptionController : BaseController
    {
        /// <summary>
        /// Set the connection
        /// </summary>
        /// <param name="conn"></param>
        public QuestionOptionController(IOptions<ReadConfig> conn)
        {
            _conn = conn;
        }

        /// <summary>
        /// Get the Questions
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuestion")]
        public async Task<IActionResult> GetQuestion()
        {
            string sqlQuery = @"SELECT distinct StepId = StapID,QuestionText = VragenTekst,QuestionId=B.VraagId from Vragen V left join BezoekRapportVraagAntwoord B on V.vraagId=B.vraagId";

            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<QuestionViewModel>(sqlQuery);
                return Ok(result);
            }
        }

        /// <summary>
        /// Get the all Questions
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllQuestion")]
        public async Task<IActionResult> GetAllQuestion()
        {
            string sqlQuery = @"SELECT Text = VragenTekst+'( Step '+cast(StapID as varchar)+')',Id=VraagId from Vragen order by StapID asc";

            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<DropDownViewModel>(sqlQuery);
                return Ok(result);
            }
        }

        /// <summary>
        /// Get the Options
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetOption")]
        public async Task<IActionResult> GetOption()
        {
            string sqlQuery = @"SELECT distinct  StepId=R.StapId,QuestionText = R.VragenTekst,QuestionId = V.VraagID,OptionText = VraagoptieTekst,FlagOptionId=B.OptieID,OptionId=V.OptieID from VraagOpties  V left join BezoekRapportVraagAntwoord B on V.OptieID=B.OptieID left join Vragen R on V.VraagID=R.VraagID";

            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<OptionViewModel>(sqlQuery);
                return Ok(result);
            }
        }

        /// <summary>
        /// Create Question
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateQuestion")]
        public async Task<IActionResult> CreateQuestion([FromBody] QuestionViewModel model)
        {

            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var parameters = new DynamicParameters();
                parameters.Add("@StapID", model.StepId);
                parameters.Add("@VragenTekst", model.QuestionText);
                parameters.Add("@RESULT", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                var result = await dbConnection.ExecuteAsync("SP_CreërenVragen", parameters, null, null, commandType: CommandType.StoredProcedure);
                int VisitReportId = parameters.Get<int>("@RESULT");
                if (result == 1)
                {
                    var viewModel = GetQuestionViewModel(model);
                    return Ok(viewModel);

                }
                else if (result == 0)
                    return NotFound();
                else
                {

                    ModelState.AddModelError("Error", Resources.DatabaseErrorControllerResources.SaveDataInDatabaseErrorMessage);
                    return BadRequest(ModelState);
                }
            }
        }


        /// <summary>
        /// delete Question
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteQuestion/{id}")]
        public async Task<IActionResult> DeleteQuestion(int id)
        {

            string sqlQuery = @"delete from Vragen where StapID=" + id + "";

            if (!string.IsNullOrEmpty(sqlQuery))
            {
                using (System.Data.IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    var result = await dbConnection.QueryAsync(sqlQuery);
                    return Ok();
                }
            }
            ModelState.AddModelError("Error", Resources.DatabaseErrorControllerResources.GetDataFromDatabaseErrorMessage);
            return BadRequest(ModelState);
        }

        /// <summary>
        /// Create Option
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateOption")]
        public async Task<IActionResult> CreateOption([FromBody] OptionViewModel model)
        {
            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var parameters = new DynamicParameters();
                parameters.Add("@VraagID", model.QuestionId);
                parameters.Add("@VraagoptieTekst", model.OptionText);
                parameters.Add("@RESULT", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                var result = await dbConnection.ExecuteAsync("SP_CreërenVraagOpties", parameters, null, null, commandType: CommandType.StoredProcedure);
                int VisitReportId = parameters.Get<int>("@RESULT");
                if (result == 1)
                {
                    var viewModel = GetOptionViewModel(model);
                    return Ok(viewModel);

                }
                else if (result == 0)
                    return NotFound();
                else
                {

                    ModelState.AddModelError("Error", Resources.DatabaseErrorControllerResources.SaveDataInDatabaseErrorMessage);
                    return BadRequest(ModelState);
                }
            }
        }

        /// <summary>
        /// delete Option
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteOption/{id}")]
        public async Task<IActionResult> DeleteOption(int id)
        {

            string sqlQuery = @"delete from VraagOpties where optieid=" + id + "";

            if (!string.IsNullOrEmpty(sqlQuery))
            {
                using (System.Data.IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    var result = await dbConnection.QueryAsync(sqlQuery);
                    return Ok();
                }
            }
            ModelState.AddModelError("Error", Resources.DatabaseErrorControllerResources.GetDataFromDatabaseErrorMessage);
            return BadRequest(ModelState);
        }

        /// <summary>
        /// Update Question
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("UpdateQuestion/{id}")]
        public async Task<IActionResult> UpdateQuestion(int id,[FromBody] QuestionViewModel model)
        {

            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var parameters = new DynamicParameters();
                parameters.Add("@StapID", model.StepId);
                parameters.Add("@VragenTekst", model.QuestionText);
                parameters.Add("@RESULT", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                var result = await dbConnection.ExecuteAsync("SP_BijwerkenVragen", parameters, null, null, commandType: CommandType.StoredProcedure);
                int VisitReportId = parameters.Get<int>("@RESULT");
                if (result == 1)
                {
                    var viewModel = GetQuestionViewModel(model);
                    return Ok(viewModel);

                }
                else if (result == 0)
                    return NotFound();
                else
                {
                    ModelState.AddModelError("Error", Resources.DatabaseErrorControllerResources.SaveDataInDatabaseErrorMessage);
                    return BadRequest(ModelState);
                }
            }
        }

        /// <summary>
        /// Update Option
        /// </summary>
        /// /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("UpdateOption/{id}")]
        public async Task<IActionResult> UpdateOption(int id,[FromBody] OptionViewModel model)
        {
            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var parameters = new DynamicParameters();
                parameters.Add("@OptieID", model.OptionId);
                parameters.Add("@VraagID", model.QuestionId);
                parameters.Add("@VraagoptieTekst", model.OptionText);
                parameters.Add("@RESULT", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                var result = await dbConnection.ExecuteAsync("SP_BijwerkenVraagOpties", parameters, null, null, commandType: CommandType.StoredProcedure);
                int VisitReportId = parameters.Get<int>("@RESULT"); 
                if (result == 1)
                {
                    var viewModel = GetOptionViewModel(model);
                    return Ok(viewModel);

                }
                else if (result == 0)
                    return NotFound();
                else
                {

                    ModelState.AddModelError("Error", Resources.DatabaseErrorControllerResources.SaveDataInDatabaseErrorMessage);
                    return BadRequest(ModelState);
                }
            }
        }


        /// <summary>
        /// GetQuestionViewModel method is use for assign value in QuestionViewModel
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private QuestionViewModel GetQuestionViewModel(QuestionViewModel item)
        {
            QuestionViewModel viewModel = new QuestionViewModel();
            viewModel.StepId = item.StepId;
            viewModel.QuestionText = item.QuestionText;
            return viewModel;
        }

        /// <summary>
        /// GetOptionViewModel method is use for assign value in OptionViewModel
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private OptionViewModel GetOptionViewModel(OptionViewModel item)
        {
            OptionViewModel viewModel = new OptionViewModel();
            viewModel.QuestionId = item.QuestionId;
            viewModel.OptionText = item.OptionText;
            return viewModel;
        }
    }
}