﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CF.PMS.Data.Models;
using CF.PMS.Data.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using CF.PMS.API.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using CF.PMS.API.Infrastructure;

namespace CF.PMS.API.Controllers
{
    /// <summary>
    /// User's API
    /// </summary>
    [Produces("application/json")]
    [Route("api/user")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Constants.Roles.Administrator)]
    public class UserController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private IDataRepository<AspNetUsers> userRepository;
        private IDataRepository<AspNetUserRoles> roleRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="userRepository"></param>
        /// <param name="roleRepository"></param>
        /// <param name="roleManager"></param>
        public UserController(UserManager<ApplicationUser> userManager, IDataRepository<AspNetUsers> userRepository, IDataRepository<AspNetUserRoles> roleRepository, RoleManager<IdentityRole> roleManager) : base()
        {
            this.roleManager = roleManager;
            this.userRepository = userRepository;
            this.roleRepository = roleRepository;
            this._userManager = userManager;
        }

        /// <summary>
        /// Get the user by given user id
        /// </summary>
        /// <param name="id">Unique Id of the user</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetId")]
        public IActionResult Get(string id)
        {
            var user = userRepository.Get(s => s.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            UserViewModel userViewModel = GetUserViewModel(user);
            return Ok(userViewModel);
        }

        /// <summary>
        /// Get the user by given user name
        /// </summary>
        /// <param name="userName">Username</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByUserName/{userName}")]
        public IActionResult GetUserByUserName(string userName)
        {
            var user = userRepository.Get(s => s.UserName == userName);
            if (user == null)
            {
                return NotFound();
            }

            UserViewModel userViewModel = GetUserViewModel(user);
            return Ok(userViewModel);
        }

        /// <summary>
        /// Create a new user with the given details
        /// </summary>
        /// <param name="model">User details</param>
        /// <returns></returns>
        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateNewUserViewModel model)
        {
            var user = new ApplicationUser
            {
                Email = model.Email,
                UserName = model.Email,
                IsActive = false
            };

            var existingRole = await roleManager.FindByNameAsync(model.RoleName);
            if (existingRole == null)
            {
                ModelState.AddModelError("", string.Format(Infrastructure.Resources.UserControllerResources.RoleNotExistingErrorMessage, model.RoleName));
                return BadRequest(ModelState);
            }

            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                var roleResult = await _userManager.AddToRoleAsync(user, model.RoleName.ToString());
                if (!roleResult.Succeeded)
                {
                    AddErrors(roleResult);
                    return BadRequest(ModelState);
                }
            }
            else
            {
                AddErrors(result);
                return BadRequest(ModelState);
            }

            var userViewModel = GetUserViewModel(user);
            userViewModel.RoleId = existingRole.Id;
            userViewModel.RoleName = existingRole.Name;
            return Ok(userViewModel);
        }

        /// <summary>
        /// Updates an existing user with the given details
        /// </summary>
        /// <param name="model">User details</param>
        /// <param name="id">User id</param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> Update([FromBody] UpdateUserViewModel model, string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            var existingRole = await roleManager.FindByNameAsync(model.RoleName);
            if (existingRole == null)
            {
                ModelState.AddModelError("", string.Format(Infrastructure.Resources.UserControllerResources.RoleNotExistingErrorMessage, model.RoleName));
                return BadRequest(ModelState);
            }

            if (!string.IsNullOrWhiteSpace(model.Password))
            {
                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var passwordResetResult = await _userManager.ResetPasswordAsync(user, token, model.Password);
                if (!passwordResetResult.Succeeded)
                {
                    AddErrors(passwordResetResult);
                    return BadRequest(ModelState);
                }
            }

            var currentRoles = await _userManager.GetRolesAsync(user);

            if (!currentRoles.Contains(model.RoleName))
            {
                await _userManager.RemoveFromRolesAsync(user, currentRoles);
                var roleResult = await _userManager.AddToRoleAsync(user, model.RoleName.ToString());
                if (!roleResult.Succeeded)
                {
                    AddErrors(roleResult);
                    return BadRequest(ModelState);
                }
            }

            var userViewModel = GetUserViewModel(user);
            userViewModel.RoleId = existingRole.Id;
            userViewModel.RoleName = existingRole.Name;
            return Ok(userViewModel);
        }

        /// <summary>
        /// Activate an existing user
        /// </summary>
        /// <param name="id">Unique Id for the user</param>
        /// <returns></returns>
        [HttpPost]
        [Route("ActivateUser/{id}")]
        public IActionResult ActivateUser(string id)
        {
            var user = userRepository.Get(s => s.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            user.IsActive = true;
            userRepository.SaveChanges();
            return Ok();
        }

        /// <summary>
        /// Deactivate an existing user
        /// </summary>
        /// <param name="id">Unique Id for the user</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeactivateUser/{id}")]
        public IActionResult DeactivateUser(string id)
        {
            var user = userRepository.Get(s => s.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            user.IsActive = false;
            userRepository.SaveChanges();
            return Ok();
        }

        /// <summary>
        /// Search users with paging
        /// </summary>
        /// <param name="userName">User name to search. It uses contains logic</param>
        /// <param name="excludeInactiveUser">Exclude inactive user from the search</param>
        /// <param name="pageNumber">The page number requesting</param>
        /// <param name="pageSize">Number of records in a page</param>
        /// <returns></returns>
        [HttpGet]
        [Route("SearchUser")]
        public IActionResult SearchUser(string userName, bool excludeInactiveUser = true, int pageNumber = 1, int pageSize = 10)
        {
            var usersQuery = from u in userRepository.GetAll()
                             join r in roleRepository.GetAll() on u.Id equals r.UserId into rs
                             from r in rs.DefaultIfEmpty()
                             select new { User = u, RoleName = r == null ? "" : r.Role.Name, RoleId = r == null ? "" : r.Role.Id };

            if (!string.IsNullOrWhiteSpace(userName))
            {
                usersQuery = usersQuery.Where(s => s.User.UserName.Contains(userName));
            }
            if (excludeInactiveUser)
            {
                usersQuery = usersQuery.Where(s => s.User.IsActive == true);
            }

            var totalRecords = usersQuery.Count();

            var pagedUsersQuery = usersQuery.OrderBy(s => s.User.UserName).Skip((pageNumber - 1) * pageSize).Take(pageSize);
            var users = pagedUsersQuery.ToList();
            List<UserViewModel> userViewModels = new List<UserViewModel>();

            foreach (var item in users)
            {
                UserViewModel userViewModel = GetUserViewModel(item.User);
                userViewModel.RoleId = item.RoleId;
                userViewModel.RoleName = item.RoleName;
                userViewModels.Add(userViewModel);
            }

            PagedEntity<UserViewModel> pagedResult = new PagedEntity<UserViewModel>();
            pagedResult.Data = userViewModels;
            pagedResult.TotalCount = totalRecords;

            return Ok(pagedResult);
        }
        /// <summary>
        /// AddErrors
        /// </summary>
        /// <param name="result"></param>
        protected void AddErrors(IdentityResult result)
        {
            foreach (IdentityError error in result.Errors)
            {
                ModelState.AddModelError(error.Code, error.Description);
            }
        }

        /// <summary>
        /// GetUserViewModel method is use for assign value for AspNetUsers in UserViewModel ...
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private static UserViewModel GetUserViewModel(AspNetUsers user)
        {
            UserViewModel userViewModel = new UserViewModel();
            userViewModel.CountryCode = user.CountryCode;
            userViewModel.Email = user.Email;
            userViewModel.EmailConfirmed = user.EmailConfirmed;
            userViewModel.Id = user.Id;
            userViewModel.IsActive = user.IsActive;
            userViewModel.PhoneNumber = user.PhoneNumber;
            userViewModel.PhoneNumberConfirmed = user.PhoneNumberConfirmed;
            userViewModel.UserName = user.UserName;
            return userViewModel;
        }

        /// <summary>
        /// GetUserViewModel method is use for assign value for ApplicationUser in UserViewModel ...
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private static UserViewModel GetUserViewModel(ApplicationUser user)
        {
            UserViewModel userViewModel = new UserViewModel();
            userViewModel.CountryCode = user.CountryCode;
            userViewModel.Email = user.Email;
            userViewModel.EmailConfirmed = user.EmailConfirmed;
            userViewModel.Id = user.Id;
            userViewModel.IsActive = user.IsActive;
            userViewModel.PhoneNumber = user.PhoneNumber;
            userViewModel.PhoneNumberConfirmed = user.PhoneNumberConfirmed;
            userViewModel.UserName = user.UserName;
            return userViewModel;
        }
    }
}