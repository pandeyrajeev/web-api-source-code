﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CF.PMS.API.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using CF.PMS.API.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Data;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.IO;
using CF.PMS.Data.Models;

namespace CF.PMS.API.Controllers
{
    [Produces("application/json")]
    [Route("api/GPS")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Constants.Roles.Administrator)]
    public class GPSController : BaseController
    {
        private ICustomLogger logger;

        /// <summary>
        /// Set the connection
        /// </summary>
        /// <param name="conn"></param>
        public GPSController(IOptions<ReadConfig> conn, ICustomLogger logger)
        {
            _conn = conn;
            this.logger = logger;
        }
        /// <summary>
        /// get records by postcode and countrycode bases from Database....
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lng"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetNearestCustomers/{lat}/{lng}")]
        public async Task<IActionResult> GetNearestCustomers(double lat, double lng)
        {
            List<NearestCustomersViewModel> viewModels = new List<NearestCustomersViewModel>();
            string strlat =  Convert.ToString(lat).Replace(',', '.');
            string strlng = Convert.ToString(lng).Replace(',', '.');
            string origin = GetOriginAddress(strlat, strlng);
            if (origin != "")
            {
                string sqlQuery = @"select CustomerId= Klantnummer ,CustomerName= Klantnaam,Address =[Adresregel 1],Postcode,CountryCode= [Landcode]   from Klanten where Postcode like '%" + origin.Split(',')[0] + "%' and Landcode ='" + origin.Split(',')[1] + "'";
                if (!string.IsNullOrEmpty(sqlQuery))
                {
                    using (System.Data.IDbConnection dbConnection = Connection)
                    {
                        dbConnection.Open();
                        var result = await dbConnection.QueryAsync(sqlQuery);

                        if (result.Count() > 0)
                        {
                            foreach (var item in result)
                            {
                                NearestCustomersViewModel viewModel = new NearestCustomersViewModel();
                                string distance = GetDistance(origin, item.Address + " " + item.Postcode + "," + item.CountryCode);
                                viewModel.CustomerId = item.CustomerId;
                                viewModel.CustomerName = item.CustomerName;
                                viewModel.Distance = distance;
                                viewModels.Add(viewModel);
                            }
                            var viewresult = viewModels.OrderBy(s => s.Distance);
                            return Ok(viewresult);
                        }
                        else
                            return Ok(viewModels);
                    }
                }
            }
            return Ok(viewModels);
        }

        /// <summary>
        /// GetOriginAddress method is use for getting the Origin address...
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lng"></param>
        /// <returns></returns>
        private string GetOriginAddress(string lat, string lng)
        {
            string laURL = @"https://maps.google.com/maps/api/geocode/xml?latlng=" + lat + "," + lng + "&sensor=true&key=AIzaSyD9KkqBqBwV1Kcq7PFGrnAzDFcVO1mBITM";
            string content = GetContents(laURL);
            string Origin = "";
            string postal_code = string.Empty;
            string country_code = string.Empty;
            XmlDocument doc = new XmlDocument();
            StringReader readXML = new StringReader(content);
            doc.Load(readXML);
            XmlNodeList nodeList = doc.SelectNodes("/GeocodeResponse/result[position() = 2]/address_component");
            if (nodeList != null)
            {
                foreach (XmlNode childNode in nodeList)
                {
                        if (childNode.SelectSingleNode("type[text()='country']") != null)
                            country_code = childNode.SelectSingleNode("short_name").InnerText;
                        if (childNode.SelectSingleNode("type[text()='postal_code']") != null)
                            postal_code = childNode.SelectSingleNode("short_name").InnerText;
                        if (!string.IsNullOrEmpty(postal_code) && !string.IsNullOrEmpty(country_code))
                        {
                            Origin = postal_code + "," + country_code;
                            break;
                        }
                }
            }
            if(string.IsNullOrEmpty(Origin))
            {
                XmlNodeList nodeListfirst = doc.SelectNodes("/GeocodeResponse/result[position() = 1]/address_component");
                if (nodeListfirst != null)
                {
                    foreach (XmlNode childNode in nodeListfirst)
                    {
                        if (childNode.SelectSingleNode("type[text()='country']") != null)
                            country_code = childNode.SelectSingleNode("short_name").InnerText;
                        if (childNode.SelectSingleNode("type[text()='postal_code']") != null)
                            postal_code = childNode.SelectSingleNode("short_name").InnerText;
                        if (!string.IsNullOrEmpty(postal_code) && !string.IsNullOrEmpty(country_code))
                        {
                            Origin = postal_code + "," + country_code;
                            break;
                        }
                    }
                }
            }
            return Origin;

        }

        /// <summary>
        /// GetDistance method is use find the distance ....
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        private string GetDistance(string origin, string destination)
        {
            System.Threading.Thread.Sleep(1000);
            string distance = "";
            string url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin + "&destination=" + destination + "&sensor=true&key=AIzaSyD9KkqBqBwV1Kcq7PFGrnAzDFcVO1mBITM";
            string content = GetContents(url);
            JObject o = JObject.Parse(content);
                distance = (string)o.SelectToken("routes[0].legs[0].distance.text");
                return distance;
        }

        /// <summary>
        /// GetContents method is use for get the result content...
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        protected string GetContents(string Url)
        {
            logger.LogInfo("Request Url:" + Url);
            string sContents = string.Empty;
            string me = string.Empty;
            try
            {
                    System.Net.WebClient wc = new System.Net.WebClient();
                    byte[] response = wc.DownloadData(Url);
                    sContents = System.Text.Encoding.ASCII.GetString(response);
                    //System.IO.StreamReader sr = new System.IO.StreamReader(Url);
                    //sContents = sr.ReadToEnd();
                    //sr.Close();
            }
            catch(Exception ex)
            {
               logger.LogError(ex.Message);
            }
            logger.LogInfo("Response Contents:" + sContents);
            return sContents;
        }
    }
}