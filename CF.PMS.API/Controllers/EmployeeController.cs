﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CF.PMS.API.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using CF.PMS.API.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Data;

namespace CF.PMS.API.Controllers
{

    /// <summary>
    /// Employee API
    /// </summary>
    [Produces("application/json")]
    [Route("api/Employee")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Constants.Roles.Administrator)]
    public class EmployeeController : BaseController
    {
        /// <summary>
        /// Set the connection
        /// </summary>
        /// <param name="conn"></param>
        public EmployeeController(IOptions<ReadConfig> conn)
        {
            _conn = conn;
        }

       /// <summary>
       /// get all employees
       /// </summary>
       /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            string sqlQuery = @"select Id=[Werknemer-ID],Text = [Voornaam]+' '+[Achternaam] from Werknemers where [Voornaam] is not null and [Voornaam] <>'.'  order by [Voornaam]";
            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<EmployeeViewModel>(sqlQuery);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
        }
    }
}