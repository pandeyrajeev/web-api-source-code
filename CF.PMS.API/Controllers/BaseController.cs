﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CF.PMS.Data.Models;
using CF.PMS.Data.IRepository;
using Microsoft.AspNetCore.Authorization;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.Text;
using CF.PMS.API.Extensions;
using System.Data.SqlClient;
using Microsoft.Extensions.Options;

namespace CF.PMS.API.Controllers
{
    /// <summary>
    /// Base controller for all the API's
    /// </summary>
    public class BaseController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        public IOptions<ReadConfig> _conn;

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseController()
        {
        }

        /// <summary>
        /// Gets the current logged in user's unique Id
        /// </summary>
        public string LoggedInUserId
        {
            get
            {
                return HttpContext.User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
            }
        }

        /// <summary>
        /// Gets the current logged in user's user name
        /// </summary>
        public string LoggedInUserName
        {
            get
            {
                return HttpContext.User.Claims.First(c => c.Type == ClaimTypes.Name).Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public System.Data.IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_conn.Value.ConnectionString);
            }
        }
    }
}
