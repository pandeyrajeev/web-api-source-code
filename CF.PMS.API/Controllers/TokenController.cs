﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CF.PMS.Data.Models;
using CF.PMS.Data.IRepository;
using Microsoft.AspNetCore.Authorization;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Identity;
using CF.PMS.API.ViewModels;
using CF.PMS.API.Infrastructure;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace CF.PMS.API.Controllers
{
    /// <summary>
    /// Token generation endpoint
    /// </summary>
    [Produces("application/json")]
    [Route("api/token")]
    public class TokenController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IConfiguration config;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="roleManager"></param>
        /// <param name="configuration"></param>
        public TokenController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.config = configuration;
        }

        /// <summary>
        /// Get the token for the provided username/pasword combination
        /// </summary>
        /// <param name="authUserRequest"></param>
        /// <returns></returns>
        [HttpPost("")]
        [AllowAnonymous]
        public async Task<IActionResult> Token([FromBody] LoginRequest authUserRequest)
        {
            try
            {
                var user = await userManager.FindByNameAsync(authUserRequest.UserName);        
                if (user != null)
                {
                    bool isValidPassword = userManager.CheckPasswordAsync(user, authUserRequest.Password).Result;
                    if (isValidPassword)
                    {
                        var claims = await GetValidClaims(user);

                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["Tokens:Key"]));
                        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                        var token = new JwtSecurityToken(config["Tokens:Issuer"],
                        config["Tokens:Issuer"],
                        claims,
                        expires: DateTime.Now.AddMinutes(60),
                        signingCredentials: creds);

                        var userRoles = await userManager.GetRolesAsync(user);
                        var rolesAsCommaSparated = String.Join(",", userRoles.Select(s => s.ToString()));

                        Dictionary<string, string> userClaims = new Dictionary<string, string>();
                        userClaims.Add("UserName", user.UserName);
                        userClaims.Add("UserId", user.Id);
                        userClaims.Add("Email", user.Email);
                        userClaims.Add("Role", rolesAsCommaSparated);
                        return Ok(new TokenViewModel
                        {
                            Claims = userClaims,
                            Token = new JwtSecurityTokenHandler().WriteToken(token)
                        });
                    }
                }
                ModelState.AddModelError("Error", Resources.TokenControllerResources.TokenErrorMessage);
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                //Log the error but never show it to user
                ModelState.AddModelError("Error", Resources.TokenControllerResources.TokenErrorMessage);
                return BadRequest(ModelState);
            }

        }

        /// <summary>
        /// Get the token for the provided passed in user Name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpPost("LoginAs/{userName}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Constants.Roles.Administrator)]
        public async Task<IActionResult> LoginAs(string userName)
        {
            try
            {
                var user = await userManager.FindByNameAsync(userName);
                if (user != null)
                {
                    var claims = await GetValidClaims(user);

                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["Tokens:Key"]));
                    var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                    var token = new JwtSecurityToken(config["Tokens:Issuer"],
                    config["Tokens:Issuer"],
                    claims,
                    expires: DateTime.Now.AddMinutes(60),
                    signingCredentials: creds);

                    var userRoles = await userManager.GetRolesAsync(user);
                    var rolesAsCommaSparated = String.Join(",", userRoles.Select(s => s.ToString()));

                    Dictionary<string, string> userClaims = new Dictionary<string, string>();
                    userClaims.Add("UserName", user.UserName);
                    userClaims.Add("UserId", user.Id);
                    userClaims.Add("Email", user.Email);
                    userClaims.Add("Role", rolesAsCommaSparated);
                    return Ok(new TokenViewModel
                    {
                        Claims = userClaims,
                        Token = new JwtSecurityTokenHandler().WriteToken(token)
                    });
                }
                ModelState.AddModelError("Error", Resources.TokenControllerResources.TokenErrorMessage);
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                //Log the error but never show it to user
                ModelState.AddModelError("Error", Resources.TokenControllerResources.TokenErrorMessage);
                return BadRequest(ModelState);
            }

        }

        private async Task<List<Claim>> GetValidClaims(ApplicationUser user)
        {
            IdentityOptions _options = new IdentityOptions();
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, user.Id.ToString()),
                //new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64),
                new Claim(_options.ClaimsIdentity.UserIdClaimType, user.Id.ToString()),
                new Claim(_options.ClaimsIdentity.UserNameClaimType, user.UserName)
            };
            var userClaims = await userManager.GetClaimsAsync(user);
            var userRoles = await userManager.GetRolesAsync(user);
            claims.AddRange(userClaims);
            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));
                //var role = await _roleManager.FindByNameAsync(userRole);
                //if (role != null)
                //{
                //    var roleClaims = await _roleManager.GetClaimsAsync(role);
                //    foreach (Claim roleClaim in roleClaims)
                //    {
                //        claims.Add(roleClaim);
                //    }
                //}
            }
            return claims;
        }
       
    }
}
