﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CF.PMS.Data.Models;
using CF.PMS.Data.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using CF.PMS.API.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace CF.PMS.API.Controllers
{
    /// <summary>
    /// Role's API
    /// </summary>
    [Produces("application/json")]
    [Route("api/roles")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RolesController : BaseController
    {
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IDataRepository<AspNetRoles> rolesRepository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleManager"></param>
        /// <param name="rolesRepository"></param>
        public RolesController(RoleManager<IdentityRole> roleManager, IDataRepository<AspNetRoles> rolesRepository) : base()
        {
            this.roleManager = roleManager;
            this.rolesRepository = rolesRepository;
        }

        /// <summary>
        /// Get all the roles in the system
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var roles = roleManager.Roles.ToList();
            List<RoleViewModel> roleViewModels = new List<RoleViewModel>();
            foreach (var item in roles)
            {
                roleViewModels.Add(new RoleViewModel
                {
                    Id = item.Id,
                    Name = item.Name
                });
            }
            return Ok(roleViewModels);
        }
    }
}