﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CF.PMS.API.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using CF.PMS.API.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Data;

namespace CF.PMS.API.Controllers
{
    /// <summary>
    /// CompetitorModel's API
    /// </summary>
    [Produces("application/json")]
    [Route("api/Competitor")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Constants.Roles.Administrator)]
    public class CompetitorModelController : BaseController
    {
        /// <summary>
        /// Set the connection
        /// </summary>
        /// <param name="conn"></param>
        public CompetitorModelController(IOptions<ReadConfig> conn)
        {
            _conn = conn;
        }

        /// <summary>
        /// Get the Competitor by given id
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetById")]
        public async Task<IActionResult> GetById(int Id)
        {
            using (System.Data.IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var parameters = new DynamicParameters();
                parameters.Add("@klant", Id);
                var result = await dbConnection.QueryAsync<CompetitorModelViewModel>("SP_GetCompetitorModel", parameters, null, null, commandType: CommandType.StoredProcedure);
                if (result ==null)
                    return NotFound();
                return Ok(result);
            }
        }
    }
}